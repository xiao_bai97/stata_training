
*    -------------------------------------------------- 
*                                                       
*         $$$$$ $$$$$$$$    $    $$$$$$$$    $          
*        $$  $$ $  $$  $   $$    $  $$  $   $$          
*        $$   $    $$      $$$      $$      $$$         
*        $$$       $$     $ $$      $$     $ $$         
*          $$$     $$     $  $$     $$     $  $$        
*           $$$    $$     $$$$$     $$     $$$$$        
*        $   $$    $$    $   $$     $$    $   $$        
*        $$  $$    $$    $    $$    $$    $    $$       
*        $$$$$    $$$$  $$$  $$$$  $$$$  $$$  $$$$      
*                                                       
*    -------------------------------------------------- 
*                                                       
*       _________________________________________       
*       —————————————————————————————————————————       
*                Stata 研讨班 (初级)               
*                        北京                           
*               (2018.1.13-2018.1.16)                 
*                                                       
*       QQ群号: 122160188 (Stata-连老师-2018寒假)         
*       _________________________________________       
*       ————————————————————————————————————————— 
*                                                       
*                                                       
*                主讲人：连玉君                   
*                                                       
*        单  位：中山大学岭南学院金融系                
*        电  邮: arlionn@163.com                        
*        主  页：http://www.lingnan.sysu.edu.cn/lnshizi/faculty_vch.asp?tn=50 
*        简  书: http://www.jianshu.com/u/69a30474ef33           
*        微  博：http://weibo.com/arlionn               
*        课  程：http://www.peixun.net/author/3.html    
     
*        微  信：lianyj45                               
*        公众号：Stata连享会(微信: StataChina)           
		            
		  
		  

*             ========================
*                第一讲  Stata 简介   
*             ========================


*-注意：执行后续命令之前，请先执行如下几条命令
  global path "`c(sysdir_personal)'\PX_A_2018a\A1_intro"  //定义课程目录
  global D    "$path\data"      //范例数据
  global R    "$path\refs"      //参考文献
  global Out  "$path\out"       //结果：图形和表格
  adopath +   "$path\adofiles"  //自编程序 
  cd "$D"
  set scheme s2color  
  *-note: 
  *   `c(sysdir_personal)' 等价于 D:\stata15\ado\personal
	
	
* ------------------
* ---- 本讲目录 ----
* ------------------
*                                  
* 1.1  本课程简介                  
* 1.2  STATA概貌                   
* 1.3  执行指令                    
* 1.4  修改资料                    
* 1.5  log 文件: 记录你的分析过程 
* 1.6  do 文档: 高效快捷地执行命令
* 1.7  Stata 设定                  


*---------------- 
*-Stata 入门资料  
  
  help                      //帮助系统目录  
  help contents_should_know //必须知道的命令
  help language             //基本语法规则
  
  shellout "$R\User_Guide.pdf" //Stata用户指南,请坚持读完
  
*-Stata 功能一览
  
  view browse "http://www.stata.com/features/"

  help contents	                //帮助文件的完整目录
      
  help contents_utilities_basic //基本命令
  
  help contents_data            //数据处理
	  
  help contents_stat            //统计和回归分析
	   
  help graph                    //绘图
	  
  help program                  //编程
	  
  help contents_matrix          //矩阵
  
  help estimation_commands  //估计命令概览
      
  help estcom               //估计和检验命令概览
  
  help guide                //一些最基本的命令和概念

  help operator             //运算符
  
  help functions            //函数
             
  help language             //Stata的语法格式
  
  help query                //系统参数及设定
  
  help winfonts             //操作界面和字体设定
   
  help contents_interface   //更全面的设定

	
*-------
*-有关实证分析过程 Data 和 Codes 的一些建议
*-Matthew Gentzkow, and Jesse M. Shapiro1, 2014.
* Code and Data for the Social Sciences:
* A Practitioner’s Guide
  shellout "$R\Gentzkow_2014_CodeAndData.pdf"

*-入门教科书 I  
*-Baum, C. 2006. 
* An Introduction to Modern Econometrics Using Stata: Stata Press.  
  shellout "$R\Baum_2006.pdf"
  
*-入门教科书 II  
*-Acock, AC, 2014. 
* A gentle introduction to stata: Stata Press.
  shellout "$R\Acock_2014.pdf"   

  
*---------------Also see--------------
*-Why use Stata statistical software? 
*-------------------------------------

  view browse "http://www.stata.com/why-use-stata/"
  
  view browse "http://www.stata.com/netcourse/intro-nc101/"

  view browse "http://data.princeton.edu/wws509/stata/c3s6.html"  //讲得很好

  view browse "http://www.cpc.unc.edu/research/tools/data_analysis/statatutorial"
  
  view browse "http://kurt.schmidheiny.name/teaching/stataguide2up.pdf"
  
  *-Book example
  view browse "http://www.ats.ucla.edu/stat/stata/examples/icda/icdast5.htm"
  
  
  
*--------------------
*-> 1.1  本课程简介
*--------------------

          *     ==本节目录==

          *     1.1.1 简介
		  *     1.1.2 课程配套资料的使用方法
          *     1.1.5 讨论和建议
 

*-------------
*-1.1.1  简介
   
		 
*-----------------------------
*-1.1.2 课程配套资料的使用方法

   *-1.1.2.1 课程配套资料的存放位置
   
     *-我们提供的压缩包：只需解压后放置于 D 盘根目录下即可
	 * 注意：是 D:\stata15, 而非 D:\stata15\stata15 或 D:\stata15
	 
	 *-若用自己的stata软件，需做如下设定:
	 
	 * (1) profile.do 文件放置于stata安装目录下，
	 *     如 D:\stata15\profile.do
	 *     注：若你已经自行设定了该文件，
	 *         请将我的profile文件合并到你的文件中
	   doedit "D:\stata15\profile.do"
	   doedit "`c(sysdir_stata)'profile.do"  //若stata未放置于D盘下，执行此条
	 
	 * (2) 重新打开 stata，若上述文件设定无误，则会显示
	 *     “running D:\stata15\profile.do ...”
	 
	 * (3) 输入 sysdir 命令，会显示如下信息
	 *
     *      STATA:  D:\stata15\
     *    UPDATES:  D:\stata15\ado\updates\
     *       BASE:  D:\stata15\ado\base\
     *       SITE:  D:\stata15\ado\site\
     *       PLUS:  D:\stata15\ado\plus\     //存放和下载外部命令的位置
     *   PERSONAL:  D:\stata15\ado\personal\ //个人文件夹


   *-1.1.2.2 如何打开本讲义 (do-files)
     
	 *-方法1：依次点击 
	 *   "New do-file editor"-->File-->Open 指向如下路径
	 *    或输入 doedit，然后点击 File-->Open
	 *     D:\stata15\ado\personal\PX_A_2018a
	 *     双击 A1_intro 即可
          
     *-方法2：依次输入如下命令
       cd "D:\stata15\ado\personal"
	   cd `c(sysdir_personal)' //等价于上面的命令,适用于Stata不在D盘下的情形
	   cd PX_A_2018a
       doedit "A1_intro.do "
	 *-or
	   doedit "D:\stata15\ado\personal\PX_A_2018a\A1_intro.do"
	   doedit "`c(sysdir_personal)'PX_A_2018a\A1_intro.do"  //等价
	   
	 
   *-1.1.2.3 关于范例数据
     
	 *-stata官方的范例数据
	 
	   help dta_contents  // (File-->Example Datasets)
	   
	   *-注：多数已经下载，存放于 D:\stata15\ado\Examples
	   * 打开方式File-->Open-->D:\stata15\ado\Examples
	   
     *-本课程的范例数据
        cd "D:\stata15\ado\personal\PX_A_2018a\A1_intro"
		lall
		ldta
   
   
*------------------
*-1.1.3 讨论和建议

   *-StataList  stata 官网的讨论区，有很多高质量的解答
     view browse "https://www.statalist.org/"

   *-人大论坛【计量版】之【STATA专版】：
     view browse "http://www.pinggu.org/bbs/forum-67-1.html"
	  
   *-人大论坛【统计软件培训班VIP在线答疑区】
   *       http://www.pinggu.org/bbs/forum-114-1.html
     view browse "http://www.pinggu.org/bbs/forum-114-1.html"
      
   * 【Arlion 的博客】http://blog.cnfol.com/arlion
   *       在百度中搜索关键词 “连玉君 博客”
     view browse "http://blog.cnfol.com/arlion"
	 
   * 【E-mail】: arlionn@163.com
   
   * 【连玉君主页】：
     view browse "http://www.lingnan.net/intranet/teachinfo/dispuser.asp?name=lianyj"


 
 
 
*--------------------
*-> 1.2  Stata 概貌  
*--------------------

  *-实证分析的流程
  *  [1] 一个想法：研究假设
  *  [2] 模型设定
  *  [3] 变量界定
  *  [4] 收集数据
  *  [5] 数据清洗、生成变量
  *  [6] 统计和回归分析
  *  [7] 支持-->[8]  |  不支持 --> [1]-[7]
  *  [8] 解释结果、结论

  *-讨论：在上述过程中，Stata 的角色和作用是什么？
  
  
*--------------------
*-1.2.1 Stata 的特点
 
   *-短小精悍

   *-运算速度极快

   *-绘图功能卓越
     cd "$D"
     do "L1_butterfly.do"           //函数图: 一只蝴蝶
	 doedit "L1_color_brand.do"     //彩色带状背景
	 
	 sysuse "nlsw88", clear
	 pvenn married collgrad south   //韦恩图
	 *-Also see
	 help venndiag
	 venndiag married collgrad south
     
   *-更新和发展速度惊人
     help 
	 findit dynamic panel data   //重要命令
	 help   whatsnew             //重要命令
	         /*
	 ado
	 mypkg
             */
   
*------------------------------
*-1.2.2 一些最基本的 Stata 命令
  
*-帮助文件和外部命令  最终的两个命令：help, findit
  help regress        //查看帮助文件,每天必用几十次！
  findit sargan       //搜索关键词,非常重要的命令
  findit synthetic control  //合成控制法
  findit winsor2
  ssc install winsor2 //从ssc下载外部命令
  
*-文件路径和设置  see [1.7.3 / 1.7.4 文件目录和文件夹]
  pwd              //显示当前工作路径
  cd "D:\stata15"  //改变当前工作路径
  cd "D:\stata15\ado\personal\PX_A_2018a\A1_intro"  //进入文件夹 
     * 建议在路径名称外加双引号,醒目
     * 通常是从电脑的 [地址栏] 中复制粘贴过来的 
  cd "$D"
  dir       //当前路径下的文件
  sysdir    //系统文件的路径
  adopath   //stata只能识别存放于这些文件夹下的程序文件
            //可以使用 [adopath +] 和 [adopath -] 来增减路径
  sysuse auto, clear  // 调入系统自带的数据文件
  sysuse dir          // 列示所有系统自带的数据文件

*-快捷键  see [1.7.6 常用快捷键]
  *-----------------------------------------------------
  *  Ctrl+D 	执行 Do文档中选中的命令   (*)
  *  Ctrl+R     在do文档中运行程序(Run)   (*)
  *  Ctrl+Tab   切换、打开、恢复窗口(尝试一下即可知道其功能)
  *  PgUp/PgDn  在不同的窗口中作用不同，尝试一下	
  *-----------------------------------------------------
			
*-调入和导入数据 see [A2_data.do --> 2.1 输入和导入数据]
  use "fullauto.dta", clear    //调入Stata格式的数据(当前路径下,相对路径)
  use "D:\stata15\ado\personal\PX_A_2018a\A1_intro\fullauto.dta", clear
                               //调入数据，绝对路径
  sysuse "auto", clear         //调入Stata自带数据文件
  shellout "auto.xls"          //打开文件,要写全后缀
  import excel "auto.xls", sheet(domestic) ///
         firstrow clear      //导入Excel数据, `///' 是断行符
  save "mydata.dta", replace //保存数据, also see [help saveold]
 
 *-----------从网页上下载资料----------------------begin-----self-reading
  local address http://quotes.money.163.com/service/chddata.html
  local field TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP 
  local name = 300398	//飞凯材料
  copy "`address'?code=1`name'&fields=`field'\\`name'.csv"  s`name'.csv, replace
  insheet using "`address'?code=1`name'&fields=`field'\\`name'.csv", clear
  shellout "s300398.csv"
 *-----------从网页上下载资料----------------------over----- 
  *-打包后的命令
  help cntrade   //从网易下载个股的日交易资料

 
*-编辑和浏览数据  
  edit                           //编辑数据, 不能修改数据
  browse                         //浏览数据,不能修改数据
  format price wei len mpg %6.3f //定义变量的显示格式
  br, nolabel                    //browse 的简写  
  
*-产生新变量和修改旧变量
  gen lnprice = ln(price)
  label variable lnprice "ln(汽车价格-price)"
  
  gen Yesbad = 0
  replace Yesbad=1 if rep78>=4
  
*-基本统计量  
  sysuse "nlsw88.dta", clear  //调入当前工作路径下的数据文件
  des                         //describe, 数据形态 
  des2                        //外部命令，很好用
/*
variable name   type    format    value label   variable label
idcode          int     %8.0g                   NLS id
age             byte    %8.0g                   age in current year
race            byte    %8.0g     racelbl       race
married         byte    %8.0g     marlbl        married
*/  

  sysuse "auto", clear     //调入Stata自带数据文件
  codebook                 //数据概览
  compress                 //自动精简资料的存储格式 
  sum                      //summarize,基本统计量
  
  fsum, s(mean sd p50 min max) cat(rep78 foreign) label  //外部命令,很好用
/*
      Variable |   N     Mean       Min      Max  Label 
---------------+--------------------------------        
         price |  74  6165.26   3291.00 15906.00  Price 
           mpg |  74    21.30     12.00    41.00  Mileage (mpg)
         rep78 |  69     3.41      1.00     5.00  Repair Record 1978
        1 (%)  |   2     2.90
        2 (%)  |   8    11.59
        3 (%)  |  30    43.48
        4 (%)  |  18    26.09
        5 (%)  |  11    15.94
      headroom |  74     2.99      1.50     5.00  Headroom (in.)
             (略)
       foreign |  74     0.30      0.00     1.00  Car type
 Domestic (%)  |  52    70.27
  Foreign (%)  |  22    29.73
*/

  bysort foreign: fsum price wei len  //分组统计
  
  logout, save("$Out\Table1_sum") word replace: ///
    tabstat price wei len mpg turn foreign,      ///
    stat(mean sd p50 min max) format(%7.2f) column(statistic) 
  * Note: (1) 上述三行要一起执行; 
  *       (2) logout 那一行的作用是把结果输出到 excel 表格中;
  *       (3) /// 是换行符 
/*
    variable |      mean        sd       p50       min       max
-------------+--------------------------------------------------
       price |   6165.26   2949.50   5006.50   3291.00  15906.00
      weight |   3019.46    777.19   3190.00   1760.00   4840.00
      length |    187.93     22.27    192.50    142.00    233.00
         mpg |     21.30      5.79     20.00     12.00     41.00
        turn |     39.65      4.40     40.00     31.00     51.00
     foreign |      0.30      0.46      0.00      0.00      1.00
----------------------------------------------------------------
*/
  
*-列示和屏幕显示  
  sort price //升序排列, help gsort
  display "差旅费 = " in green 1450*2 + 360*3
  list price foreign in 1/20, sepby(foreign) //屏幕列示观察值
  *-Also see
    help rsort  // randomly sort, 把数据打乱,随机排序,外部命令
	help gsort  //可以逆序排列,从大到小
  clear         //清空内存中的数据和变量标签，但矩阵等不受影响
  clear all     //清空内存中的所有内容
  
  sysuse "auto", clear
  list price wei len mpg make
  cls           //清屏, Clear Results window, New in stata15
                //测试结果时很实用，便于区分出两次不同的运行结果
  #review 10    //显示在command窗口中新近输入的10条命令
  
*-相关系数矩阵  
  pwcorr   price wei len mpg     //相关系数矩阵, 官方命令
  pwcorr_a price wei len mpg     //相关系数矩阵, 自编命令
  graph matrix price wei len mpg //相关系数矩阵(散点图)
  
*-直方图和密度函数图  
  histogram price      //直方图
  kdensity  price      //核密度函数图
  twoway (kdensity price if foreign==1)  ///
         (kdensity price if foreign==0), ///
		 legend(label(1 "Foreign") label(2 "Domestic"))
  mkdensity price, over(foreign)  //外部命令，很好用
  scatter price weight //散点图
  twoway (scatter price weight) (lfit price weight) //散点图+线性拟合图
  
*-类变量列表分析  
  count if price>10000 //计数
  tab foreign          //列表呈现频数
  tab foreign rep78    //二维列表
/*
           |    Repair Record 1978
  Car type |   1    2    3    4    5 |  Total
-----------+-------------------------+-------
  Domestic |   2    8   27    9    2 |     48 
   Foreign |   0    0    3    9    9 |     21 
-----------+-------------------------+-------
     Total |   2    8   30   18   11 |     69 
*/  
  tab foreign rep78, sum(price) mean
/*
         |              Repair Record 1978
Car type |       1          2          3          4          5 |    Total
---------+-----------------------------------------------------+---------
Domestic | 4,564.5  5,967.625  6,607.074  5,881.556    4,204.5 | 6,179.25
 Foreign |       .          .  4,828.667  6,261.444  6,292.667 |6,070.143
---------+-----------------------------------------------------+---------
   Total | 4,564.5  5,967.625  6,429.233    6,071.5      5,913 |6,146.043
*/

*-均值差异检验
  ttest price, by(foreign)
/*
Two-sample t test with equal variances
-----------------------------------------------------------------------
   Group | Obs       Mean    Std. Err.  Std. Dev.  [95% Conf. Interval]
---------+-------------------------------------------------------------
Domestic |  52   6072.423    429.4911   3097.104   5210.184    6934.662
 Foreign |  22   6384.682    558.9942   2621.915    5222.19    7547.174
---------+-------------------------------------------------------------
combined |  74   6165.257    342.8719   2949.496   5481.914      6848.6
---------+-------------------------------------------------------------
    diff |      -312.2587    754.4488             -1816.225    1191.708
-----------------------------------------------------------------------
    diff = mean(Domestic) - mean(Foreign)                  t =  -0.4139
Ho: diff = 0                              degrees of freedom =       72
                                                                       
    Ha: diff < 0              Ha: diff != 0             Ha: diff > 0   
 Pr(T < t) = 0.3401      Pr(|T| > |t|) = 0.6802      Pr(T > t) = 0.6599
*/ 
 
  ttable2 price mpg wei len, by(foreign)
/*
------------------------------------------------------------------------
Variables   G1(Domestic)     Mean1   G2(Foreign)    Mean2      MeanDiff 
------------------------------------------------------------------------
price         52           6072.423     22        6384.682   -312.259   
mpg           52             19.827     22          24.773     -4.946***
weight        52           3317.115     22        2315.909   1001.206***
length        52            196.135     22         168.545     27.589***
------------------------------------------------------------------------
*/
  
*-变量名称和修改观察值 see [1.4 修改资料] 
  lookfor "Repair"     //搜索包含特定关键词的变量
  rename weight wei    //单个变量更名
  rename (length gear_ratio) (len gr) //批量重命名
  drop turn gr make    //删除变量
  drop if rep78==.                //删除观察值
  replace price=15000 if price>15000
  
*-帮助文件及外部命令的获取  see [1.7.1 Stata帮助]
  help                 //Advice on finding help
  help regress         //查看Stata内部命令的帮助文件
  findit dynamic panel //搜索关键词,下载外部命令
  ssc install winsor2, replace //直接下载 ssc 网站的外部命令
  usepackage xtabond2, update  //安装或更新外部命令 ss 
  *-Note: 本课程中涉及的所有命令都可以用 hemp cmdname 查看帮助
  
*--------------  
*-基本回归分析  see [A4_Regress.do] 

  sysuse "nlsw88", clear
  global y "wage"          //存放被解释变量的全局暂元
  global x "hours tenure married collgrad"  //help macro, 存放解释变量
  
  *-去除缺漏值
    qui reg $y $x i.race i.industry i.occupation
	keep if e(sample)
  
  *-呈现并输出基本统计量(`///' 是换行命令)
    logout, save("$Out\Table1_sum01") excel replace:  ///
            tabstat $y $x, column(stats) format(%6.2f) ///
            stats(mean sd min p50 max)  
			
  *-呈现相关系数矩阵
    logout, save("$Out\Table2_corr") excel replace: ///
            pwcorr_a $y $x
			
  *-回归分析：基本版本
    reg $y $x           //OLS 回归, basic model
      est store m1      //存储回归结果  
    reg $y $x i.race    //i.race表示种族虚拟变量, help fvvarlist
      est store m2
    reg $y $x i.race i.industry
      est store m3
    reg $y $x i.race i.occupation
      est store m4 
    *-列表呈现回归结果  see [A4_Regress, 4.7 回归结果呈现]
	local s "using $Out\Table3.csv"  //指定存储结果的 Excel 文档名称
	local m "m1 m2 m3 m4"
    esttab `m' `s', replace nogap compress ///
         ar2 scalar(N)  b(%6.3f)           ///
         star(* 0.1 ** 0.05 *** 0.01)      ///
		 noomit  nobase                    ///
		 indicate("行业效应=*.industry" "职业效应 =*.occupation")
			
                                                            /*
--------------------------------------------------------------
                 (1)          (2)          (3)          (4)   
                wage         wage         wage         wage   
--------------------------------------------------------------
hours          0.061***     0.062***     0.049***     0.037***
              (5.35)       (5.46)       (4.34)       (3.19)   
tenure         0.140***     0.146***     0.115***     0.135***
              (6.53)       (6.82)       (5.37)       (6.44)   
married       -0.293       -0.537**     -0.474*      -0.506** 
             (-1.19)      (-2.15)      (-1.92)      (-2.08)   
collgrad       3.347***     3.231***     3.528***     2.674***
             (12.17)      (11.75)      (12.33)       (8.26)   
2.race                     -1.273***    -1.097***    -0.504*  
                          (-4.67)      (-4.07)      (-1.86)   
3.race                      0.263        0.020        0.276   
                           (0.24)       (0.02)       (0.27)   
_cons          4.099***     4.538***     3.443**      7.608***
              (8.41)       (9.17)       (2.51)      (12.72)   
行业效应          No           No          Yes           No   
职业效应          No           No           No          Yes   
--------------------------------------------------------------
N               2209         2209         2209         2209   
adj. R-sq      0.106        0.114        0.155        0.181   
--------------------------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01

                                                             */
 
 
  *-Q: 记不住这些命令咋办?  help cmd
  *-A: 点菜单，命令会自动呈现在屏幕上(只能偶尔为之)
  *-B: 记笔记 (最好写到 dofile 中)
  *-C: 用 Stata 完成一篇论文 (实践证明, 这是最有效的方式)
  
  *-详情参见 Stata连享会-简书 推文 
  * 1 [Stata：回归结果中不报告行业虚拟变量的系数]
      view browse "https://www.jianshu.com/p/85f09d645862"
  * 2 [君生我未生！Stata - 论文四表一键出]
      view browse "https://www.jianshu.com/p/97c4f291ee1e"
	  
  *-Also see:
    help asdoc     // 非常快捷、综合的一个命令
	help sum2docx  // 将描述性统计量表直接输出到一个 docx 文件中
    help corr2docx // 将相关系数矩阵直接输入到一个 docx 文件中
    help t2docx    // 将分组均值t检验结果导出到一个 docx 文件中
    help reg2docx  // 将回归结果导出至 docx 文件中，用法类似于 esttab
	
	
*----------------------------------
*-1.2.3 首次使用STATA的一些基本设定   (self-reading) 
	   
   *-stata15 对中文的支持问题
   *
   *  -[Results]窗口
   *    Edit-->Preference-->General Preference Results Color 
   *    选择 "Classic" 或 "Custom2", 将所有 Bold 方框中的对号都去掉；
   *    如此可以保证-Results-窗口中的中文字符得以正常显示
   
   *  -[help viewer]窗口
   *    Edit-->Preference-->General Preference Viewer Color 
   *    选择 "Custom 1"
   *    去掉所有 "Bold" 前面的对勾，如此可保证help文件正常显示
   
   
   *-初次使用时界面偏好的设定 
   
      help window manage
	  
   * -设定方法【菜单】  
   *    Edit-->Preference-->General Preference 按喜好设定
   *    注：可进一步设定图形偏好、do-editor的风格等
   
   * -保存设定
   *    Edit-->Preference-->Save...-->New... 命名,如 Lian12
	
   * -调入已有的界面偏好设定： 
   *    Edit-->Preference-->Load...-->选择你喜欢的设定
	   

   *-阅读 stata15 电子手册
   *
   *    请将stata15手册(16个pdf文档)放置于 D:\stata15\utilities
   *    使用方法1：Help > PDF Documentation 可打开整个PDF帮助
        help regress 
   *    help regress --> [Title] [R] regress (点击此链接可以打开 PDF 手册)
   *    帮助文件右上角的三个菜单亦可点开尝试一下
        

   *-profile.do 文件: 每次启动时需要立刻执行的命令
   
	 doedit "D:\stata15\profile.do" //详见：1.11.2 小节 

	   
   
   
*---------------------------
*-1.2.3  用 Stata 作一篇论文 (简要介绍, self-reading 为主)

  *-------------------Paper 1----------------------
  *                                                
  * 叶德珠,连玉君,黄有光,李东辉.                  
  *    "消费文化、认知偏差与消费行为偏差".        
  *    经济研究, 2012(2): 80-92.                   
  *------------------------------------------------
  
   global LIAN "$path\Ye_Lian_JJYJ2012"  //存储该文文件的文件夹
   shellout "$LIAN\连玉君_2012_消费文化.pdf"     //原文
   doedit   "$LIAN\Ye_Lian_JJYJ2012.do"           //Stata分析过程

   
  *-------------------Paper 2----------------------------
  *                                                      
  * Rauh, J. D., A. Sufi, 2010,                          
  *   Capital structure and debt structure,              
  *   Review of Financial Studies, 23 (12): 4242-4280.   
  *------------------------------------------------------
   global SUFI "$path\Sufi_2010_RFS"  
   shellout "$SUFI\refs\Sufi_2010_RFS.pdf"   // 原始论文 
   doedit   "$SUFI\Sufi_2010_RFS.do"         //Stata分析过程
	
	
  *-------	
  *-Note: 很多期刊都提供原始数据和实证分析过程codes
  *-------	
  *-ICPSR datasets
    view browse "http://www.icpsr.umich.edu/icpsrweb/index.jsp" 
  *-Milne Library Data Collections: Open Data Sets by topic
    view browse "http://libguides.geneseo.edu/data"
  *-JFE datasets
    view browse "http://jfe.rochester.edu/othdat.htm"
	view browse "http://jfe.rochester.edu/data.htm"
  *-Compendium Project dataset
    view browse "http://www.isadiscussion.com/view/0/datasets.html"

	
 
 
*-------------------
*-> 1.3   执行命令
*-------------------
	  

*     ==本节目录==
         
*     1.3.1 概览
*     1.3.2 命令的适用范围
*         1.3.2.1 列举多个变量
*         1.3.2.2 样本范围的限制
*     1.3.3 命令作用的增减：使用选项 
*     1.3.4 命令的前缀         
         
*-------------
*-1.3.1  概览
  
  *--------  
  *-1.3.1.1 stata 命令的通用格式: 
  *
  *   command varlist [if] [in] [ , options] 
  *
  * [if] [in] 用于限制样本范围 
  * [options] 为“选项”，增加了命令的弹性
  
    help summarize              // 解读帮助文件
	help regress
  
  *-e.g.
    cd "$D" 
    sysuse "nlsw88", clear
    sum wage hours ttl_exp if race==2, detail
	bysort married: reg wage tenure hours i.race
	tabstat wage hours married, by(race) s(mean) f(%3.2f) 
    /*
      race |      wage     hours   married
    -------+------------------------------
     white |      8.08     36.90      0.70
     black |      6.84     38.12      0.47
     other |      8.55     36.81      0.69
    -------+------------------------------
     Total |      7.77     37.22      0.64
    --------------------------------------
    */
	
  *-特别提醒：
    * (1) "[ ]" 为可选项，可以不填，但不在[]中的内容都必须填写
    * (2) 整个命令“裸露”的逗号只有一个，此前为命令主体，此后为选项
    *     虽然选项中可能有子选项，但子选项前的逗号并未“裸露”
    * 例如：
      sysuse "sp500", clear
      twoway line close date, title("收盘价", place(left))

	    
  *--------  
  *-1.3.1.2 命令和选项的缩写
  
    sysuse "nlsw88.dta", clear
    summarize wage grade collgra ttl_exp tenure, detail
	
	*-等价于
    su w *gr* ttl-ten, detail
	
	*-Rules ([U], pp.85, Section 11.2)
	  help summ
	* 下划线部分表示能够缩写的程度;
	* 
	* 变量名称的简写, 参见：
	  help varlist
	  
	  	
*----------------------
*-1.3.2 命令的适用范围  [if] [in]
  
    help operator  //运算符
	
	sysuse "nlsw88", clear
    sum in 10/20                   // 第10至第20个观察值之间的观察值
    sum wage in -5/-1                  // 倒数...
    sum wage hours if race == 1        // 等于
    sum wage if race ~= 3              // 不等于
    sum wage if (race==2)&(married==1) // 且
    sum wage if (race==3)|(married==0) // 或
    sum wage if hours >= 40            // 大等于


*--------------------------------
*-1.3.3 使用选项：命令作用的增减

    sum wage , d
    
    *-说明：stata支持多数命令和选项的缩写，
    *       帮助文件中带下滑线的部分表示可以缩写的程度
    
    sysuse "sp500", clear
    replace volume = volume/1000
   *--------------------------------------------------------------Begin
    #delimit ;
        twoway (rspike hi low date)
               (line   close  date)
               (bar    volume date, barw(.25) yaxis(2))
                in 1/57
          , yscale(axis(1) r(900 1400))
            yscale(axis(2) r(  9   45))
            ylabel(, axis(2) grid)
            ytitle("股价 -- 最高, 最低, 收盘",place(top))
            ytitle("交易量 (百万股)", axis(2) bexpand just(left))
            xtitle(" ")
            legend(off)
            subtitle("S&P 500", margin(b+2.5))
            note("数据来源: 雅虎财经！");
    #delimit cr
	graph export "$Out\sp500_rspike.wmf", replace
   *--------------------------------------------------------------Over

	
*------------------
*-1.3.4 命令的前缀
  
    help prefix
  
  * prefix: command varlist [if] [in] [ , options] 
	
	sysuse "nlsw88", clear
	
  *-quietly	
	qui sum wage, detail
	gen mean = r(mean) 
	
	qui reg wage hours tenure ttl_exp i.race i.industry
	esttab, s(N r2_a) nogap compress
	
	
  *-by, bysort
    
	help by
	
	bysort race: tabstat wage hours tenure,  ///
	             s(mean sd min max) c(s) f(%6.3f)
				 
	bysort industry: reg wage hours tenure 			 
				 
    bysort industry married: egen av_wage = mean(wage)
	gen d_wage = wage-av_wage //经过 "行业-婚姻状况"调整后的工资水平
  
  
  *-statsby
  
    help statsby
	
	cd "$D"
	use "$D\xtcs", clear
	xtset code year
	
	*-The model:
    *
    *        D.Lev[t] = a + b*D.Lev[t-1] + e[t]
    *-or
    *     X[t]-X[t-1] = a + b(X[t-1]-X[t-2]) + e[t]
	
    statsby _b[L.D.tl], by(code) saving("$D\Lev_speed", replace): ///
	        reg D.tl L.D.tl
			
	use "$D\Lev_speed.dta", clear
	rename _stat_1 speed
	sum speed, d
	histogram speed
	count if speed<0
	
	*------------------------Reference----Figure 2--------------------
    * -Opler,1999,JFE- 
    *    Opler, T., L. Pinkowitz, R. Stulz, R. Williamson, 1999, 
    *    The determinants and implications of corporate cash holdings, 
    *    Journal of Financial Economics, 52(1): 3-46.
    *-----------------------------------------------------------------
	 shellout "$R\Opler_1999.pdf"  // pp.19, Fig3
	
	
	
	
*-------------------
*-> 1.4  修改资料       (self-reading)
*-------------------

         *     ==本节目录==
         
         *     1.4.1 变量名称和引用
		 *           1.4.1.1 新变量的名称
		 *           1.4.1.2 列举多个变量
		 *           1.4.1.3 滞后项\前移项\和差分项		 
         *     1.4.2 变量的创建和修改
         *     1.4.3 数学表达式


*----------------------
*-1.4.1 变量名称和引用

    help varname

  *--------
  *-1.4.1.1  新变量的名称

    *-新变量的基本规则：
    * (1) 由英为字母、数字或 _ 组成，至多不超过 32 个；
    * (2) 首字母必须为 字母 或 _ ；
    * (3) 英文字母的大写和小写具有不同的含义；
  
    *-示例：  
    *  abc_1 a1 _a2 _Gdp_ 都是合理的变量名
    *  5gdp 2invest 则不是；
  
    *-特别注意：
    *  建议不要使用 “_” 作为变量的第一个字母，
    *  因为许多stata的内部变量都是以 _ 开头的，
    *  如，_n, _N, _cons, _b 等等。
       help _variables

	   
  *--------
  *-1.4.1.2  列举多个变量(varlist)
    
	help varlist
    
	sysuse "nlsw88", clear
	sum age race married never_married grade
    sum age-grade    // 顺序出现的变量，列出头尾两个变量即可
    sum s*           // "*" 是孙悟空，可以表示`任何'长度的字母或数字
	sum *arr*        // 可以用在任何位置
    sum ?a?e         // "?" 是猪八戒，只能替代`一个'长度的字母或数字 
	
	
  *--------
  *-1.4.1.3  滞后项\前移项\和差分项
    
	help varlist
	
	sysuse "sp500", clear
	tsset date
	gen t = _n
	tsset t
	gen lnclose = ln(close)  // ln(收盘价)
	gen ret = D.lnclose      // 收益率,一阶差分
	gen L1ret = L1.ret       // 一期滞后项
	gen L2ret = L2.ret       // 两期滞后项
	corr *ret
	reg ret L(1/3).ret F(1/3).ret //可以不必生成，回归中直接引用即可	
                                                 /*
    -----------------------------------------------
      ret |      Coef.   Std. Err.      t    P>|t| 
    ------+----------------------------------------
      ret |
      L1. |   .0589292   .0648752     0.91   0.365 
      L2. |  -.0378565   .0651169    -0.58   0.562 
      L3. |   .0296062   .0631965     0.47   0.640 
      F1. |   .0590308   .0652264     0.91   0.366 
      F2. |  -.0373543   .0655522    -0.57   0.569 
      F3. |   .0384071   .0655662     0.59   0.559 
          |
    _cons |  -.0004479   .0008561    -0.52   0.601 
    -----------------------------------------------
                                                  */
	
	
*--------------------------
*-1.4.2 变量的创建和修改

    sysuse "nlsw88.dta", clear
	   
  *-gen 命令: help generate 
	help math functions
      gen ln_wage = ln(wage)      // 对数变换
      gen age_sq  = age^2         // 二次项
      gen dum_black = (race==2)   // 虚拟变量
	  gen tenure_black = tenure*dum_black //交乘项
	  
  *-重命名  
    help rename   
	help rename group
	  rename never_married no_marr             //单个变量重命名
	  rename (industry occupation) (indu occu) //批量重命名
	  
  *-修改观察值，egen 命令 (功能非常强大) 
    help replace
	help egen
	  gen hard_work = 0
	  bysort indu: egen median_hours = median(hours) //行业中位数
	  replace hard_work = 1 if (hours>median_hours)
	  replace hard_work = . if hours==.  //这个很重要
	help egenmore  // stata user 提供的扩展 egen 函数
	
  *-添加变量标签
    help label
	  label var ln_wage		"ln(wage)"
	  label var dum_black	"1=blak; 0=otherwise"
	  label var tenure_bl	"tenure*dum_black"	  
	  
  *-增加“数字-文字对应表”
	  sort wage
	  gen Gwage = group(3)
	  br wage Gwage
	  tab Gwage
	  label define Gwage 1 "低" 2 "中" 3 "高"  //label value 可以与变量同名
	  label value Gwage Gwage
	  label list Gwage  //显示特定变量的数字-文字对应关系
      /*
      Gwage:
                 1 低
                 2 中
                 3 高
      */	  
	  labelbook Gwage   //方法二: 显示所有变量的数字-文字对应关系
	  des2 Gwage        //方法三：更容易记忆
	  
  *-重新设定变量的显示顺序
	  des   //此前的显示顺序
	  order ln_wage hours age*  //只是改变变量在[变量窗口,右侧]的显示顺序
	  des   //此后的显示顺序
	  ds    //只列出变量名称而已
	
	
  *-一个有用的外部命令: 一次性定义变量和增加标签
    
	help cmpute     // Stata Journal 13-4
	
	sysuse "nlsw88.dta", clear
	gen lnwage = log10(wage)  //已经生成了一个变量，但不满意
	cmpute lnwage = ln(wage), replace label("ln(wage)")
	
	
*------------------
*-1.4.3 数学表达式

  * 三类：关系运算；逻辑运算；算术运算
  
    help operator  //运算符
	
  * 关系运算符  ==;  >;  <;  >=;  <=;  !=;  ~=
    sysuse "auto", clear
    list price if foreign == 0
    sum  price if foreign != 1

  * 逻辑运算符： & -->(与) ;  | -->(或)
    sysuse "auto", clear
    sum price wei if (foreign==1 & rep78<=3)
    sum price wei if (rep78==1) | (rep78==5) | (foreign !=0)
    sum price wei if (rep78>2 & rep78<5) | (price>10000)

  * 算术运算符：+ - * / ^(幂)
    display 5^2
    dis     1 - 3*2 + 4/5 - 9^3 
    dis     2*_pi 	



	

*----------------------------------
*-> 1.5 log 文件: 记录你的分析过程  
*----------------------------------
	  
  *-这部分内容了解一下即可
  * 我已经在 profile.do 文档中进行了设置，每当你打开 stata 后，
  * 会自动打开一个以时间为名称的 log 文件，存放于：
  * D:\stata15\do 文件夹下

  * 记录你的分析过程: log 文件 

    help log

	*------------------------------------开始记录-------------
	 cap log close
     log using mypaper.log, text replace  
	
	  *-调入数据
        *cd "D:\stata15\ado\personal\PX_A_2018a22\A1_intro\data"
        use "fullauto.dta", clear
	  *-Table 1
        tabstat price weight length mpg foreign
	  *-Table 2
	    pwcorr  price weight length mpg foreign 
	  *-Table 3
	    reg     price weight length mpg foreign
		est store m0
		reg     price weight length mpg if foreign==1
		est store m_f
		reg     price weight length mpg if foreign==0
		est store m_d
		esttab m0 m_f m_d, mtitle("All Foreign Domestic") ///
		       b(%4.3f) nogap compress replace            ///
			   s(N r2_a) star(* 0.1 ** 0.05 *** 0.01) 
     log close                           // 
    *------------------------------------结束记录------------
	 shellout mypaper.log

	  
    *-一些具有特殊功能的命令
	  
	  *-可以输出图形的log文件，PDF格式
	  help graphlog  //Stata Journal, 2015, 15-2: 594–596
	  
	  *-transform a Stata text log into a markup document
	  help log2markup
	
	
	
	
*------------------------------------
*-> 1.6  do 文档: 高效快捷地执行命令      (self-reading)
*------------------------------------
	 

*     ==本节目录==

*     1.6.1 do 文档简介
*         1.6.1.1 打开 do 文档编辑器
*         1.6.1.2 保存和关闭
*         1.6.1.3 执行 do 文档
*     1.6.2 合理规划你的do文档
*         1.6.2.1 一些基本规则
*         1.6.2.2 注释语句
*         1.6.2.3 断行
*         1.6.2.4 大型 do 文档的设定
*     1.6.3 列印文字
*         1.6.3.1 -display-命令 
*         1.6.3.2 列印的颜色  
*     1.6.4 Project Manager

      
*------------------   
*-1.6.1 do 文档简介 

*-> ==概览==

   *- do 文档实际上是Stata命令的集合，方便我们一次性执行多条stata命令;
   *- do 文档的使用使我们的分析工作具有可重复性；
   *- 在一篇文章的实证分析过程中，我们通常将数据的分析工作写在 do 文档中
   
   
  *-1.6.1.1 打开 do 文档编辑器
  
    *- 方法 1：
       doedit             // 打开 do-editor
       doedit "mylog.do"    // 打开一个已存在的 do 文档，可指定完整路径        

    *- 方法 2：
    *  点击 Rusults 窗口上方倒数第六个按钮            
    
    *- 设置属性：
       * Edit --> Preferences 
       * 建议选中 [Auto-indent] 和 [Save before do/run]

	   
  *-1.6.1.2 保存和关闭

    *-时刻保留你的 do-file, 用它记录所有的处理过程;
	*-中间过程产生的数据不必保存，只需执行相应的 do-files 即可
	

  *-1.6.1.3 执行 do 文档
    
    *-Case1：执行一部分命令  
      *  选中需要执行的命令，点击doedit窗口中第二行倒数第一个图标。 
      *  【快捷键】：Ctrl+D
    
    *-Case2：整体执行
         
    
*--------------------------   
*-1.6.2 合理规划你的do文档
   
  *-1.6.2.1 一些基本规则
   
    *-A. 提高 do 文档的可读性
    *
    *  gen z = z + y    is better than   gen z=z+y
    *
    *  gen z = x^2      is better than   gen z = x ^ 2
    *
    *  gen t = hours + minutes/60 + seconds/3600  
	*  is better than 
    *  gen t = hours + minutes / 60 + seconds / 3600
    *
    *  list price if (foreign==1) & (rep78>3)  
	*  is better than
    *  list price if foreign==1&rep78>3
   
    *-B. 断句和断行
	*
    *  每一行的语句不要太长，不用拖动下方导引条即可阅读；
    *  各段代码采用一个或多个空行加以分隔；
   
   
  *-1.6.2.2 注释语句
    
     help comments
    
    *-示例：
        * 第一种注释方式
		sysuse "auto.dta", clear
        sum price weight    /* 第二种注释方式 */
        gen x = 5           // 第三种注释方式
    
    
   *-1.6.2.3 断行 
     
     *-三种方式： “///” 、 “/* */” 、 #delimit 命令
     
       *-第一种断行方式： ///
         sysuse "auto", clear 
         twoway (scatter price weight)       ///
                (lfit price weight),         ///
                title("散点图和线性拟合图")
               
       *-第二种断行方式： /* */
         twoway   (scatter price weight)      /*
               */ (lfit price weight),        /*
               */ title("散点图和线性拟合图")   
              
       *-第三种断行方式： #delimit 命令
         #delimit ;
           twoway (scatter price wei)
                  (lfit price wei),
                  title("散点图和线性拟合图");
         #delimit cr
		 
         *-Note: #delimit 可以简写为  #d , 例如:
		 #d ;
		    des;  sum;  reg price wei len;
		 #d cr
		 
		 
		 
*-----------------
*-1.6.3  列印文字 

     help display
     
  *-1.6.3.1 -display-命令
    
    dis 3 + 5*7 + sqrt(20)
	
    dis  sin(_pi*0.5) + cos(0.9)
	
    dis _n(2) _dup(3) "I Love This GAME! "
    
    * 将文字置于 " " 或 `" "' 之间
       display "This is a pretty girl!"
       dis     `"This is a "pretty" girl!"'
  
  *-1.6.3.2 列印的颜色
    
    * 颜色1：red green yellow white
      dis in green "I like stata!"
      dis in w     "This " in y "is " in g "a " in red "pretty" in g " girl"
    
    * 颜色2：as text(绿色)| as result(黄色)| as error(红色)| as input(白色)
      dis as result "Stata is Good !"
     
     
  *-1.6.3.3 列印的位置
  
     * ------------------------------------------
     *   副命令  |             定义                   
     * ------------------------------------------             
     *   _col(#) | 从第 # 格开始列印
     *   _s(#)   | 跳过 # 格开始列印
     *   _n(#)   | 从第 # 行开始列印
     *   _c      | 下次列印解着列印而无须从起一行
     *   _dup(#) | 重复列印 # 次
     * ------------------------------------------
     
       display "Stata is good"
       display _col(12) "Stata is good"
       display "Stata is good" _s(8) "I like Stata"
       display _dup(3) "Stata is good！ "
       display "Stata is good","I like it"
       display "Stata is good",,"I like it"
       display _n(3) "Stata is good"
     
     * 更精美的列印方式
       help smcl       
       
     * -display-的一个妙用：清屏
       display _newline(100)   

	 
 
*-----------------------
*-1.6.4 Project Manager (stata15新增功能)

        
   *-stata15 以前的做法
   
     * 设定一个主文件，下设 N 个子文件，分别处理某一部分分析工作
     * 保存在同一个文件夹下
     
       doedit "L1_main.do"

   *-使用 Project Manager

     *-目的: 将同一个 project 相关的文档汇总到一个文档下，便于管理
	 *-适用于: 论文dofile管理；课题或课程dofile管理
	 *
	 *-新建一个 Project: 
	 *  [1] 打开 Do-file Editor, 
	 *  [2] 依次点击 File --> New --> Project...., 保存
	 *-打开一个 Project: 
	 *  [1] 打开 Do-file Editor, 
	 *  [2] 依次点击 File --> Open--> Project....
	 
	 
		  
*--------------------
*->  1.7 Stata 设定       (self-reading)
*--------------------
	  

*     ==本节目录==

*     1.7.1 Stata帮助
*     1.7.2 Stata 外部命令的获取
*           1.7.2.1 外部命令的存储路径 
*           1.7.2.2 外部命令的获取方式
*           1.7.2.3 外部命令的管理和更新
*     1.7.3 文件目录
*     1.7.4 文件和文件夹的操作
*           1.7.4.1  文件的基本操作：查找、复制、编辑和删除
*           1.7.4.2  使用stata打开txt, Word, Excel, 网页文件
*           1.7.4.3  文件夹的操作
*     1.7.5 每次启动时均需执行的命令(profile)
*     1.7.6 常用快捷键
*     1.7.7 退出stata(exit)
         
         
*-----------------
*-1.7.1 Stata帮助    -help-, -search-, -hsearch-, -findit-

  * -help-命令
  * -findit-命令   搜索内部和外部命令，最常用
  
    help regress
    search panel data, net
    hsearch "fixed effect"
    findit panel unit root
	
  *-其他相关命令(不常用)：  
	* -search-命令   searches the [keywords] of the help files; 
    * -hsearch-命令  searches the help files [themselves].

  * -view- 命令  新开窗口显示
  
    view search panel data, net        // 新开窗口显示结检索果
    view news                          // 显示stata的最近动态
    view browse "http://www.baidu.com" // 打开网页
    viewsource winsor.ado              // 查看 ado 文件源文件，只读
	adoedit winsor.ado                 // 查看 ado 文件源文件，可编辑
    viewsource xtreg_fe.ado
    viewsource xtbalance.ado
  
  
  *-更多的帮助和讨论
  
    *- 常见问题解答：FAQ  
       view browse "http://www.stata.com/support/statalist/faq"
    *- 加入STATA用户邮件列表
       view browse "http://www.stata.com/statalist/"
    *- 人大经济论坛【stata专版】
       view browse "http://www.pinggu.org/bbs/forum-67-1.html"
    *- 人大经济论坛【VIP答疑专区】
       view browse "http://www.pinggu.org/bbs/forum-114-1.html"
	*- 连玉君老师的优酷视频--stata公开课
	   view browse "http://i.youku.com/arlion"

	   
*------------------------------
*-1.7.2  Stata 外部命令的获取   

* -findit-, -ssc-, -net-, -adoupdate-, -mypkg-, -usepackage-

  *-1.7.3.1 外部命令的存储路径  
  
    *-说明：
    *  (1) 默认情况下，stata会在 "...\stata15\ado\plus" 文件夹下存储外部命令
    *  (2) 可通过 -sysdir set- 命令更改之
    *  (3) 第一次下载外部命令时，stata会自动建立 \plus 文件夹
    
    sysdir
	adopath  //这些路径下的stata命令才能够被识别
	
	*-可以自行添加或删除授权stata能够查询ado文件的路径
       
       adopath + "D:\mypaper\my_ado"      // 增加新的查询目录
       adopath - "D:\mypaper\my_ado"      // 取消特定查询目录
    
	
  *-1.7.3.2 外部命令的获取方式

    *-findit-命令：模糊查询
      findit panel data
      findit propensity score
 
    *-ssc-命令：安装(卸载)来源于 ssc 的命令
	*           ssc: Statistical Software Components
      help ssc             // http://www.repec.org/
      
	  *-显示最近一个月内最新发布的命令(很实用)
	    ssc new
		
	  *-下载量最大的 ssc 外部命令
	    ssc hot, n(30)    //下载排名前 30 
		
      *-查看来源于 SSC 的外部命令列表
        ssc describe b    // 列示以 -b- 开头的所有命令，可为 a-z,以及 "_"
        ssc describe x
        ssc des winsor2    
		
      *-下载安装 ssc 命令              
        ssc install winsor2, replace
		
	  *-复制 ssc 文件到特定文件夹中
        help ssc copy 


    *-net-命令
      help net
      *
      *-示例
              net search hausman test
         view net search hausman test
              net from http://fmwww.bc.edu/RePEc/bocode/m/   
			                            // [result]窗口显示SSC命令
         view net from http://fmwww.bc.edu/RePEc/bocode/m/   
		                                // 新开窗口显示
      *
      *-Stata Journal(SJ) 相关文档
        view net from "http://www.stata-journal.com/" 
        view net from "http://www.stata-journal.com/software/"
        net cd software  // 网络不好时，可能无法连接
        net cd sj9-2 
      *   
      *-Stata Technical Bulletin(STB) 相关文档    
        net from "http://www.stata.com/stb/"


  *-1.7.3.3 外部命令的管理和更新
  
    *-查询已安装的外部命令  -ado-, -mypkg-, -which-
	   ado
       ado, find(winsor)
       ado, find(panel unit)
       mypkg         // 呈现本机上已安装的外部命令 net findit ssc
       mypkg xt*
       mypkg *lorenz*
       mypkg xtbalance
       which xtbalance  
       which outreg2              // 列示命令的基本信息  
	   
	*-下载或更新外部命令
	   help usepackage
	   
    *-外部命令的更新     -adoupdate-
       adoupdate                  // 更新本机上的外部 ado 命令
       adoupdate outreg2, update  // 更新特定的命令 

    *-发布自己的 stata 命令
	   help usersite



*------------------
*-1.7.3  文件目录   

    *-stata 系统目录的设定
  
      sysdir        // 显示当前系统目录的设定

      *- 释义：
       *    STATA:  D:\stata15\			     stata 安装根目录
       *  UPDATES:  D:\stata15\ado\updates\	【更新文件】的存储地址
       *     BASE:  D:\stata15\ado\base\	【官方命令】存储地址
       *     SITE:  D:\stata15\ado\site\	【自编命令】存储地址
       *     PLUS:  D:\stata15\ado\plus\	【外部命令】的储存地址
       * PERSONAL:  D:\stata15\ado\personal\【自有文件夹】安装时，需自建			
  
    *- 查看
	   pwd             // 当前工作路径
       personal        // 显示路径(个人文件夹)
       personal dir    // 查看详情
       
    *- 设定   help sysdir  
       sysdir set PLUS      "D:\stata15\ado\plus" //外部命令的存放地址
       sysdir set PERSONAL  "D:\stata15\ado\personal"     //个人文件夹 
	   
	   help creturn##directories     //存放于暂元中的系统路径
       
	   

*---------------------------- 
*-1.7.4  文件和文件夹的操作

* 相关命令：shell, shellout, findfile, erase, 
*           mkdir, rmdir, copysource, winexec
 
  *-1.7.4.1  文件的基本操作：查找、查看、复制、编辑和删除
    
    findfile xtreg_fe.ado   //查找文件
    copysource xtreg_fe.ado //在adopath路径下查找,复制到当前工作目录下
    dir xt*.ado                   // 显示当前工作目录下的文件
    viewsource xtreg_fe.ado       // 查看指定的 ado 文档(只读)
    doedit "`c(pwd)'\xtreg_fe.ado"  // 编辑指定的 ado 文档
    erase  "`c(pwd)'\xtreg_fe.ado"  // 删除文件

  *-1.7.4.2  使用stata打开-.txt-, -Word-, -Excel-, -iexplorer- 文件
  
    * 语法： 
    * shellout 完整文件名        // help shellout
      
    *-打开记事本  
      shellout "d1.txt"
    *-打开-Word-文档  
      shellout "mypaper.doc"        
    *-打开-Excel-文档 
      shellout  "d1.xls"
    *-打开网页
      shellout "myhome.mht"
	  

  *-1.7.4.3  文件夹的操作
    
    *-stata官方命令  -dir-, -mkdir-, -rmdir-
      dir               // 显示当前目录下的所有文件
      dir *.txt         // 显示后缀为 ".txt" 的所有文件
      dir xt*           // 显示以 "xt" 开头的所有文件   
      mkdir `c(pwd)'\mystata   // 在当前工作路径下新建文件夹
      rmdir mystata            // 删除文件夹 
                   
    *-dirtools- 命令: 高效管理文件的外部命令
      cd  "D:\stata15\ado\personal\PX_A_2018a"
      lall              // 列示所有文件
      cd  A1_intro
      ldta              // 列示 .dta 数据文件
      cd "`c(sysdir_stata)'ado\base\x"
      lado              // 列示 .ado 文件
	 
	*-cdout- 命令：打开当前工作路径所在的文件夹
	  cd "D:\stata15\docs"  //stata 全套电子说明书存放于此
	  cdout
	  cd  "D:\stata15\ado\personal\PX_A_2018a"
	  cdout 
	  
	*-其他命令
	  help mvfiles   //文件夹的批量复制和移动
	  help backup    //自动备份重要文档，可以写入 profile
	  
	  
	  
*---------------------------------
*-1.7.5  每次启动时均需执行的命令     -profile-

  help profile

  * 建立一个 profile.do 文档，存于 D:\stata15\ 下

  * --------begin profile.do------------
  *
  *   基本参数设定
      set type double
      set memory 50m
      set matsize 2000
      set scrollbufsize 50000   // 设定屏幕的最大显示行数
      set more off,perma
	  
  *   log 文件设定
      log using    "D:\stata15\ado\personal\stata.log, text replace"
      cmdlog using "D:\stata15\ado\personal\command.log, append"
	  
  *   文件目录设定
      sysdir set PLUS     "D:\stata15\ado\plus"   //外部命令的存放地址
      sysdir set OLDPLACE "D:\ado"
      sysdir set PERSONAL "D:\stata15\ado\personal"       //个人文件夹
	  
  *   ado文档查找路径  
      adopath + "D:\stata15\ado\personal"
      adopath + "D:\stata15\ado\personal\_Myado"
	  
  *   当前工作路径
      cd "D:\stata15\ado\personal"
	  
  * --------end profile.do------------


  *- Arlion 的 profile.do 文档
  
    *doedit D:\stata15\profile.do
     doedit "`c(sysdir_stata)'profile.do"
    
	*-我的日志文件
	  cd "D:\stata15\do"
	  cdout


*---------------------  
*-1.7.6  常用快捷键    
    
  help keyboard	
                                        /*
	  F-key 	Definition
	---------------------------
	  F1 		help
	  F2 		#review;
	  F3 		describe;  (*)
	  F7 		save
	  F8 		use
	---------------------------
	
	  Ctrl-key 		Definition
	----------------------------------------------
	  Ctrl+D 		执行(Do)选中的命令 (*)
	  Ctrl+R        运行程序(Run)      (*)
	  Ctrl+F		在do-editor中搜索特定的关键词
	  Ctrl+O		打开do文档
	  Ctrl+N		新建do文档
	  Ctrl+S		保存do文档         (*)
	----------------------------------------------	
	注：上述快捷键仅适用于 do-editor
                                        */
                                        
                                        
*---------------------  
*-1.7.7  退出stata：     -exit-
  
  *-几个需要注意的事项:
   
    *- 常规方法
       * 点击叉号关闭stata，多数情况下都无需保存；
       
    *- 命令方法
       exit
       exit,  clear

