


> [连享会](https://gitee.com/arlionn)-直播平台上线了！大家可以随时随地充电了！             
> <http://lianxh.duanshu.com>            
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

> ##### 推文导航： 📍 [连享会主页](https://www.lianxh.cn)  | 📍 [知乎专栏](https://www.zhihu.com/people/arlionn/) | 📍 [公众号推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) 

&emsp; 

---
> ### 连享会-直播课

---
- **E.** &#x1F4D7; [直播-经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)  （**New！**）    
  嘉宾：李兵 (中山大学), 2020年2月28(周五)，19:00-21:00. [「课程详情」](https://www.lianxh.cn/news/761e6bbfe07a8.html)
---
- **D.** &#x1F535; [直播-空间计量全局模型及Matlab实现](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), **随时观看**，嘉宾：范巧。[「课程详情」](https://www.lianxh.cn/news/6fdb88905419e.html)
- **C.** &#x1F34E; [直播-动态面板数据模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), **随时观看**，嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/594aa12c096ca.html)
- **B.** &#x1F34F; [直播-实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)，**随时观看**，嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/2f31aa3347e83.html)     
- **A.** &#x1F36A; [直播-文本分析与爬虫专题](https://gitee.com/arlionn/Course/blob/master/Done/2020Text.md)，**2020.3.28-29， 4.4-5**，嘉宾：司继春, 游万海。[「课程详情」](https://www.lianxh.cn/news/88426b2faeea8.html)      
- **O.** &#x1F4D7; [公开课-直播-直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)，嘉宾：连玉君，**Free**. [「课程详情」](https://gitee.com/arlionn/PanelData) 
#### 课程特色
- **深入浅出**：掌握最主流的实证分析方法，获取最新重现代码和文档
- **电子板书**：全程电子板书，课后分享；听课更专注，复习更高效。
- **电子讲义**：分享全套电子版课件 (数据、程序和论文)，课程中的方法和代码都可以快速移植到自己的论文中。

#### 温馨提示

- **听课方式：** 可以通过手机、iPad、电脑在线观看、学习。附带 6 篇 Top 期刊的 Stata 实现过程，涉及 PSM+DID，RDD 等方法。
- **课程咨询：** 李老师-18636102467（微信同号）
- **课件：** 报名后点击课程主页中的「**课程表**」查看。








---

> &#x1F34E; [最新专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || &#x1F34F; [推文汇总](https://www.zhihu.com/people/arlionn/)  || &#x1F49B; [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![&#x1F4D7; 点我 - 更多推文](https://images.gitee.com/uploads/images/2020/0208/231937_2473238d_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)
