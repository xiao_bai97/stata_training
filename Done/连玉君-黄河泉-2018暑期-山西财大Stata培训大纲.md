


## 山西财大暑期 Stata 培训课程安排

[toc]



### 课程概况
- **时间地点：** 2018年8月3-8日，山西财经大学（太原）
- **授课内容：** 实证研究方法与 Stata 应用
- **授课安排：** 每天六小时，答疑 0.5 小时。
- **授课嘉宾：** 黄河泉教授 (淡江大学)、连玉君副教授 (中山大学)

  
## 授课嘉宾简介

### 黄河泉

**黄河泉**，美国范德堡大学 (Vanderbilt University, Nashville, TN) 经济学博士 (主攻计量经济、副攻财务金融)，目前为 (台湾) 淡江大学财务金融学系专任教授 (个人网站： http://mail.tku.edu.tw/river/ )。主讲课程包括：计量经济学、时间序列，财金资料探勘 (硕班) 与高等应用计量 (博班) 等课程。主要研究方向为应用计量经济，应用方面主要为宏观财务经济，这几年逐渐转往财务金融 (公司治理与公司理财) 方面之应用，以中国为主要分析对象。已经于 Journal of Development Economics, Journal of Empirical Finance 等国际期刊发表超过 50 篇学术文章 (其中有超过 40 篇收录于 SSCI 期刊)。此外，也活跃于经管之家的 Stata 论坛 (http://bbs.pinggu.org/forum-67-1.html)，回答许多 Stata 之相关问题。 



### 连玉君

**连玉君**，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“计量分析与Stata应用”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文60余篇。连玉君副教授主持国家自然科学基金项目（2项）、教育部人文社科基金项目、广东自然科学基金项目等课题项目10余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。




### 授课内容

---
#### A 班 - 黄河泉 (主讲)

> **1. 论文知道无他，求其 t 值而已矣。但你的 t 值对吗？(I)** 

  - 最小二乘法与 t 统计量之回顾。
  - 稳健标准误：异方差、序列相关等处理 (t 值之分母问题)
  - Stata 时间：
    - Stata 工作路径结构 (do, dta, excel, log 等)
	- WDI 之资料与使用 
	- dofile 的使用
	- 导入和合并数据
	- 基本统计和回归表格和创建和导出    
Note: 写一个 do 档，将下载之 excel 档，输入 Stata并 reshape 与储存，merge 合并各 Stata 资料档，最后建构各种表格，包括基本统计量，相关系数矩阵与一般回归结果。

> **2. 工具变法** 

  - 论文知道无他，求其 t 值而已矣。但你的 t 值对吗？(II)
  - 何谓内生性？其来源和影响为何？ (t 值之分子问题) 
  - 工具变量 (instrumental variable, IV) 之介绍与使用
  - Stata 时间：介绍 ivreg2 之应用与解读
  
> **3. 面板数据模型 (RE, FE)** 

  - 介绍固定效果 (fixed effect, FE) 与随机效果 (random effect, RE) 模型与估计
  - 模型筛选：要用 FE 或 RE？ 同方差与异方差下之 Hausman 检定
  - 期刊文章的真相 (firm, industry, and year fixed effects 与 robust or cluster at firm/industry level)。
  - Stata时间：介绍 xtreg, areg, reghdfe, xtivreg2, ivreghdfe 等常用并好用之命令
  - SSCI 时间：Bruckner, Markus (growth), Lin, Faqin (openness)
  
> **4. Panel ARDL 模型** 

  - 允许定态与非定态之变量。
  - 可以區隔長、短期效果 (方法)。
  - Stata 时间： pmg/mg 
  - SSCI 时间：解读 SSCI 论文中该方法的使用 (范文 2 篇)
  
> **5. 倍分法 (Difference-in-Differences, DID)** 

  - 相同之政策改变时点。
  - 不同之政策改变时点。
  - Stata 时间： diff, xtreg 或 reghdfe 等
  - SSCI 时间：范文 2 篇
  
> **6. 倾向得分匹配分析 (propensity score matching，PSM)** 

  - PSM (+ DID)
  - EB (entropy balancing) 与 CEM (coarsened exact matching) 
  - Stata时间：psmatch2, ebalance 与 cem 等命令
  - SSCI时间：范文 2 篇。

----
#### B 班 - 连玉君 (主讲)

1. 随机边界分析 (SFA) I：SFA 简介、效率的估算和分析、异质性 SFA
1. 随机边界分析 (SFA) II: 双边 SFA、面板 SFA、内生性 SFA
1. 断点回归分析 (RDD)
1. 合成控制法 (SCM)
1. 论文写作和课题标书
1. 一篇论文的重现过程讲解

  

> **第 1-2 讲 随机边界分析 (SFA)**

- **内容简介：** 本讲全面介绍了目前日益得到广泛应用的随机边界模型（SFA），包括异质性 SFA、面板 SFA 和双边 SFA，是产出效率、成本效率、议价能力、投资效率估算等领域的主要分析工具。异质性随机边界模型有助于我们在估算出无效率程度的同时，可以进一步分析影响非效率程度的因素。在前期文献中，固定效应面板 SFA 模型的估计是一直是个难题，Greene (2005) 提出了 TFE-SFA（True fixed effect SFA）模型，Wang and Ho (2010) 提出了缩放因子TFE-SFA模型，本课程将提供上述两个模型的Stata估计程序，能够很方便地估计包含固定效应的面板SFA模型。双边随机边界模型（Two-tier SFA）由Kumbhakar, Tsionas and Sipil inen (2009) 提出，在衡量信息不对称程度、投资效率、议价能力等方面有重要应用。

- **授课内容：**
   - 传统的 SFA 模型（Traditional SFA）
   - SFA 的模型设定和估计方法
   - 异质性 SFA 模型（Heterogeneity SFA）
   - 面板 SFA
     - Greene (2005), True Fixed Effect (TFE-SFA) 模型
     - Wang and Ho (2010), SP-SFA模型
   - 双边SFA模型（Two-tier SFA）
   - **范例：** 卢洪友, 连玉君, 卢盛峰. 中国医疗服务市场中的信息不对称程度测算. **经济研究**, 2011(4): 94-106. 

> **第 3 讲 断点回归分析 (RDD)**

- **何时使用断点回归分析？** 
  - RDD 是目前公认的 **最为干净** 的政策评价方法之一。以 **「上北大是否有助于提高收入？」** 这一问题为例。
  - **传统处理方法**：在收入方程中控制其他影响收入的因素后，重点考察 **D=1 (北大学生)；D=0 (其他学生)** 这一虚拟变量的系数。然而，这一做法难逃自我选择偏误的影响。因为，是否上北大并非随机选择的结果，很可能是能力强的孩子更容易上北大，而能力强的孩子也更容易找到高收入的工作 (即使不上北大也是如此)。因此，即使发现北大学生毕业时的年薪比其他学生高 5 万，也不能将其全部归因为北大教育带来的效果。比如，5=3+2，意味着北大教育的收入效应是 3 万，而个人能力的收入效应是 2 万。当然，也有可能是 5  = 1+4，甚至是 5 = -2+7。难点在于，个人能力是不可观测的。
  - **RDD：** 假如上北大的分数线是 670 分，为了克服上述自选择偏误对估结果的影响，我们可以重点分析 [668-669] 分数段的学生 (未上北大-**对照组**) 与 [670-671] 分数段的学生 (上北大-**实验组**) 在毕业时的收入差异。由于单个学生无法左右北大的录取分数线，所以这里的实验组和对照组可以视为随机分组。同时，由于两者的分数差异被限定在一个很小的范围，我们可以认为他们各方面的特征也比较相近。此时，二者的收入差异可以归因为北大教育带来的效果。
  - 本例中，高考分数称为分配变量，分数线 672 分称为分配点或断点 (cut-point)，而毕业后的收入则成为结果变量。我们把断点看成一种 treatment，断点右侧的是处理组，断点左侧的是控制组。如果结果变量在断点两侧发生了显著的跳跃 (jump)，则认为政策有效。**想一想：RDD 的合理性依赖于哪些假设条件？**
  ![断点回归分析图示](https://upload-images.jianshu.io/upload_images/7692714-f97edfe5f8e524ea.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- **授课内容**
  - 断点回归分析(RDD)	Regression Discontinuity Design (RDD) 简介
  - 明确断点RDD分析 (Sharp RDD)
  - 模糊断点RDD分析 (Fuzzy RDD)
  - 范例：2篇文章


> **第 4 讲** **合成控制法 (Synthetic control methods)**

- **何时使用合成控制法？**
在政策评价领域，长期存在着一个空白地带：有些政策只针对某一个省或某一个州实施，此时 DID，PSM 等方法都不再适用，因为我们只有一个孤零零的实验对象！Abadie, Diamond, and Hainmueller (2010, JASA) 提出的**合成控制法**，很好的解决了这类问题。他们研究了「加州」于1989年实施的禁烟法案的政策效果。其基本思想是，使用其他38个未实施禁烟法案的州的加权平均来虚构一个 **「合成加州」**，进而对比分析 **「加州」** 和 **「合成加州」** 在1989年之后香烟消费量的差异，这就是政策效果！作者还进一步的使用的组合检验，以及 **「安慰剂检验」** 来确定这一结果的统计显著性。
![合成控制法图示 - Abadie_2010_Figure 2 ](https://upload-images.jianshu.io/upload_images/7692714-2f9f2f9519b036b4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- **授课内容**
  - 合成控制法简介
  - 精讲一篇经典论文(Stata实现过程)：[1] Abadie, A., A. Diamond, J. Hainmueller, 2010, Synthetic control methods for comparative case studies: Estimating the effect of california's tobacco control program, ***Journal of the American Statistical Association***,  105 (490): 493-505\. [[PDF](https://pan.baidu.com/s/16KHY4Wz9lrbh3t19WHzwXQ)]
   - 以一篇中文文献为例，介绍该模型的具体应用：[2] 苏治, 胡迪, 2015, 通货膨胀目标制是否有效?——来自合成控制法的新证据, 经济研究, (6): 74-88\. [[PDF](https://pan.baidu.com/s/1kpMyiS-jas4iq-sKn4qrrQ)]
   - 亮点：样本中有多个国家实施了通胀目标制，作者多次使用SMC，分别对每个国家进行分析；同时，作者对SMC相对于DID和PSM的优劣进行了详细的论述。

>**第 5 讲 论文写作和课题标书**

- 授课内容：
   - 论文的选题、文献综述 (Endnote的使用)
   - 研究贡献的挖掘和陈述
   - 研究设计(模型设定和筛选)、数据和变量
   - 修改报告的撰写 (与审稿人有效沟通)
   - 课题选题和子课题的设定
   - 研究基础、研究目标、研究内容、研究难点
   - 特色和创新点的提炼
   - 标书的结构和标书的修改
   - 经验分享：一份自科基金标书

>**第 6 讲 重现经典论文：公司资本结构的动态调整行为 | Flannery and Rangan(2006)**

- Flannery, M. J., K. P. Rangan, **2006**, Partial adjustment toward target capital structures, ***Journal of Financial Economics***, 79 (3): 469-506. [PDF](https://gitee.com/arlionn/stata_training/raw/master/Refs/Flannery_2006.pdf)
- **文章简介：** 资本结构的动态调整行为并不是一个新的研究话题，但他一直在持续被讨论。这篇论文以权衡理论为基础，在部分调整模型架构下研究了美国上市公司资本结构的动态调整行为。作者发现，若不考虑公司的个体效应，则调整速度会被严重低估，为此，他们使用了固定效应模型 (**FE**)，估得的调整速度约为前期文献的三倍，达到 0.38。问题在于，这一结果可信吗？如果使用更为合理的 **FD-GMM** 进行估计，结果又将如何呢？
- **方法：** 
  - 混合 OLS (Pooled OLS)
  - Fama-MacBeth 估计 (FM method)
  - 固定效应模型 (FE)
  - 动态面板模型 (Dynamic Panel data)
  - 各种稳健性检验设计
- **亮点：** 
  - 实证分析过程值得借鉴，非常严谨的研究设计
  - 对**衡量偏误**、**均值回复**、不同理论的对比检验、模型设定等棘手问题的处理很巧妙
  - 该文引发了后续多个方向的研究，我们将梳理几个主要的方向，探求论文的选题和研究方向问题。
- **主题：** 资本结构动态调整（权衡理论、优序融资理论、市场择时理论）

## 报名信息

- **时间地点：** 2018年8月3-8日，山西财经大学（太原）
- **授课内容：** 实证研究方法与 Stata 应用
- **授课安排：** 每天六小时，答疑 0.5 小时。
- **授课嘉宾：** 黄河泉教授 (淡江大学)、连玉君副教授 (中山大学)
- **费用（含报名费、材料费；食宿自理）**：
  - 单人价：4000元/人
  - 团报价：3500元/人（五人及以上）
- 联系方式：
  - 邮箱：wjx004@sina.com
  - 电话 (微信同号)：王老师 18903405450 ；李老师 ‭1863610246
- **报名链接：**[**http://m.eqxiu.com/s/uUbyoECf**](http://m.eqxiu.com/s/uUbyoECf)
- **公众号：** 可搜索公众号名称（君泉Stata）或扫描二维码关注公众号了解详情
- **温馨提示：** 现接受预报名（7.31日前），预报名者：3900元/人，7 月份正式缴费（对公账号届时通过短信方式通知，或现场刷卡缴费，按预报名顺序排座位）
- 


**地点：** 山西财经大学国际学术交流中心

**时间：** 上午9:00-12:00，下午14:00-17:00(17:00-17:30答疑)

**授课方式：** 采用Stata15软件，中文多媒体互动授课方式

**报名咨询：**
  - 王老师：18903405450（微信同号）
  - 邵老师：15536063856（微信同号）
  - 李老师：18636102467（微信同号）

**会议详情**
**主办方：** 太原君泉教育咨询有限公司
**适合人群：** 主要面向人文社会科学领域的高校教师、机构研究者、硕博群体等。
**费用**（含报名费、材料费）**：** 全价：4000元/人
团报价（五人及以上）：3500元/人
**报名方式：长按识别二维码报名**
![image](http://upload-images.jianshu.io/upload_images/7692714-660b23cbe3cee429?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

报名链接
![image](http://upload-images.jianshu.io/upload_images/7692714-d6074430b07b1757?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

微信客服报名（李老师）
![image](http://upload-images.jianshu.io/upload_images/7692714-b38fa8392e5fd657?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

微信客服报名（王老师）


**注：现接受预报名（7月份前），预报名者：3900元/人，7月份正式缴费（对公账号届时通过短信方式通知，或现场****刷卡****缴费，按预报名顺序排座位）**
